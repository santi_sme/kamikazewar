//
//  MainMenuViewModel.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 24/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import Foundation

final class MainMenuViewModel {
    
    // MARK: - Public Properties
    let router: MainMenuRouter
    weak var view: MainMenuViewController?
    
    // MARK: - Inits
    init(router: MainMenuRouter) {
        self.router = router
        
    }
    
}

// MARK: - Extension MainMenuViewControllerProtocol
extension MainMenuViewModel: MainMenuViewControllerProtocol {
    func loadHightScore() -> Int {
        return HightScore.load()
    }
    
    func saveHightScore(score: Int) {
        HightScore.save(score)
    }
    
    func presentGame() {
        router.presentGame()
    }
}

// MARK: - Extension MainMenuRouterProtocol
extension MainMenuViewModel: GameRouterDelegate {
    func updateHightScore(score: Int) {
        let hightScore = loadHightScore()
        if (score > hightScore) {
            saveHightScore(score: score)
            view?.updateHightScore(score: score)
        }
    }
}

// MARK: - Protocols
protocol MainMenuViewModelProtocol {
    func updateHightScore(score: Int)
}
