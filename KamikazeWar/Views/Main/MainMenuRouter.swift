//
//  MainMenuRouter.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 24/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import UIKit

final class MainMenuRouter {
    // MARK: - Public Properties
    weak var viewController: MainMenuViewController?
    weak var delegate: GameRouterDelegate?
    
    // MARK: - Methods
    static func configureModule() -> UIViewController {
        let router = MainMenuRouter()
                
        let viewModel = MainMenuViewModel(router: router)
        
        let viewController = MainMenuViewController(viewModel: viewModel)
        
        viewModel.view = viewController
        router.viewController = viewController
        router.delegate = viewModel

        return viewController
    }
    
}

// MARK: - Extensions
extension MainMenuRouter {
    func presentGame() {
        guard let delegate = self.delegate else {
            return
        }
        
        let gameVC = GameRouter.configureModule(delegate: delegate)
        viewController?.present(gameVC, animated: true)
    }
}

//// MARK: - Extension GameRouterDelegate
//extension MainMenuRouter: GameRouterDelegate {
//    func updateHightScore(score: Int) {
//        guard let delegate = self.delegate else {
//            return
//        }
//        delegate.updateHightScore(score: score)
//    }
//}

