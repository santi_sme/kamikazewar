//
//  MainMenuViewController.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 15/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import UIKit

final class MainMenuViewController: UIViewController {
    
    // MARK: - Public Properties
    let viewModel: MainMenuViewModel
    
    init(viewModel: MainMenuViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Outlets
    @IBOutlet weak var hightScoreLabel: UILabel!

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    // MARK: - Actions
    @IBAction func startGameTapped(_ sender: UIButton) {
        viewModel.presentGame()
    }
    
    // MARK: - Private Methods
    private func setupUI() {
        DispatchQueue.main.async { [weak self] in
            let hightScore = self?.viewModel.loadHightScore()
            self?.hightScoreLabel.text = "Hight Score: \(hightScore ?? 0)"
        }
    }
    
}

// MARK: - Protocols
protocol MainMenuViewControllerProtocol {
    func loadHightScore() -> Int
    func saveHightScore(score: Int)
    func presentGame()
}

// MARK: - Extension MainMenuViewModelProtocol
extension MainMenuViewController: MainMenuViewModelProtocol {
    func updateHightScore(score: Int) {
//        let hightScore = viewModel.loadHightScore()
//        if (score > hightScore) {
        self.hightScoreLabel.text = "Hight Score: \(score)"
//            viewModel.saveHightScore(score: score)
//        }
    }
    
}
