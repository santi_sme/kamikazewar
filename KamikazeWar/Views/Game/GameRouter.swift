//
//  GameRouter.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 24/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import UIKit

final class GameRouter {
    // MARK: - Public Properties
    weak var viewController: GameViewController?
    weak var delegate: GameRouterDelegate?
    
    // MARK: - Methods
    static func configureModule(delegate: GameRouterDelegate) -> UIViewController {
        let router = GameRouter()
                
        let viewModel = GameViewModel(router: router)

        let viewController = GameViewController(viewModel: viewModel)
        
        viewModel.view = viewController
        router.viewController = viewController
        router.delegate = delegate
        
        return viewController
    }
}

// MARK: - Protocols
extension GameRouter: GameViewModelProtocol {
    func updateHightScore(score: Int) {
        self.delegate?.updateHightScore(score: score)
    }
}

protocol GameRouterDelegate: class {
    func updateHightScore(score: Int)
}

//protocol MainMenuRouterDelegate: class {
//    func updateHightScore(score: Int)
//}
