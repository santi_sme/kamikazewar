//
//  GameViewController.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 18/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import UIKit
import ARKit

class GameViewController: UIViewController {

    // MARK: - Public Properties
//    weak var delegate: GameViewControllerProtocol?
    let viewModel: GameViewModel
    
    // MARK: - Outlets
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var ammo1Button: UIButton!
    @IBOutlet weak var ammo1Label: UILabel!
    @IBOutlet weak var ammo2Button: UIButton!
    @IBOutlet weak var ammo2Label: UILabel!
        
//    init(viewModel: GameViewModel, delegate: GameViewControllerProtocol) {
    init(viewModel: GameViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        runScene()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pauseScene()

    }
    
    // MARK: - Actions
    @IBAction func exitTapped(_ sender: UIButton) {
        showExitAlert()
    }
    
    @IBAction func ammo1Tapped(_ sender: UIButton) {
        sender.isEnabled = true
        self.viewModel.ammo1Tapped()
        self.ammo2Button.layer.borderWidth = 0
        self.ammo1Button.layer.borderWidth = 2

    }
    
    @IBAction func ammo2Tapped(_ sender: UIButton) {
        sender.isEnabled = true
        self.viewModel.ammo2Tapped()
        self.ammo1Button.layer.borderWidth = 0
        self.ammo2Button.layer.borderWidth = 2
    }
    
    @IBAction func fireTapped(_ sender: UITapGestureRecognizer) {
        guard let camera = self.sceneView.session.currentFrame?.camera else {
            return
        }
        
        let bulletNode = Bullet(camera)
        
        self.sceneView.scene.rootNode.addChildNode(bulletNode)
        
        // Descontamos la munición de tipo 2 si procede
        if (self.viewModel.damageMultiplier > 1) {
            self.viewModel.subAmmo2()
            self.ammo2Label.text = "\(self.viewModel.ammo2)"
            
        }
        
        // Cuando la munición tipo 2 llega a 0, deshabilitamos el botón y pasamos a munición 1
        if (self.viewModel.ammo2 <= 0) {
            self.ammo2Button.isEnabled = false
            self.viewModel.setDamageMultiplier(to: 1)
            self.ammo2Button.layer.borderWidth = 0
            self.ammo1Button.layer.borderWidth = 2
        }
    
    }
    
    // MARK: - Private Methods
    private func showExitAlert() {
        let alert = UIAlertController(title: title, message: "Please confirm if you really want to exit", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] _ in
            self?.gameOver()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [weak self] _ in
            self?.runScene()
        }

        alert.addAction(yesAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) { [weak self] in
            self?.pauseScene()

        }
    }
    
    private func setupUI() {
        setupAmmoButtons()
        sceneView.session.delegate = self
        
        // Debug
//        sceneView.debugOptions = .showWireframe
        sceneView.autoenablesDefaultLighting = true
        
        runScene()
        
        // Delegado que informa de cuando ocurre una colision
        sceneView.scene.physicsWorld.contactDelegate = self
        
        addNewShip(count: 2)
    }
    
    private func setupAmmoButtons() {        
        let redCircle = drawAmmoButton(fillColor: .red, rect: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.ammo1Button.setBackgroundImage(redCircle, for: .normal)
        let blueCircle = drawAmmoButton(fillColor: .blue, rect: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.ammo2Button.setBackgroundImage(blueCircle, for: .normal)        
        let grayCircle = drawAmmoButton(fillColor: .gray, rect: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.ammo2Button.setBackgroundImage(grayCircle, for: .disabled)

        self.ammo1Button.setTitle("X1", for: .normal)
        self.ammo2Button.setTitle("X2", for: .normal)

        self.ammo1Label.text = "Infinite"
        self.ammo2Label.text = "\(self.viewModel.ammo2)"
        
        self.ammo1Button.isEnabled = true
        self.ammo2Button.isEnabled = (self.viewModel.ammo2 > 0) ? true : false
        
        self.ammo1Button.layer.borderColor = UIColor.yellow.cgColor
        self.ammo1Button.layer.borderWidth = 2
        self.ammo2Button.layer.borderColor = UIColor.yellow.cgColor
        self.ammo2Button.layer.borderWidth = 0

    }
    
    func drawAmmoButton(fillColor: UIColor = UIColor.gray, rect: CGRect) -> UIImage {
        // Begin new Image Context to start drawing
        UIGraphicsBeginImageContext(rect.size)

        // set the color
        fillColor.setFill()

        // draw the path
        let circle = UIBezierPath(roundedRect: rect, cornerRadius: rect.width/2)
        let newContext = UIGraphicsGetCurrentContext()
        newContext?.saveGState()
        newContext?.addPath(circle.cgPath)
        newContext?.drawPath(using: .fill)
        
        // grab an image of the context
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()

        UIGraphicsEndImageContext()

        return image
    }
    
    private func runScene() {
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)

    }
    
    private func pauseScene() {
        self.sceneView.session.pause()
    }
    
    private func addNewShip(count: Int) {
        for _ in 1 ... count {
            let avion = Ship()
            let xAddition = CGFloat.random(in: 0 ... 1)
            let x = CGFloat.random(in: -2.5 ... 2.5) - xAddition
            let yAddition = CGFloat.random(in: 0 ... 1)

            let y = CGFloat.random(in: 0 ... 1.5) - yAddition
            let z = CGFloat.random(in: -2.5 ... -1.5)

            avion.position = SCNVector3(x, y, z)
            self.viewModel.addAvion(avion: avion)
            avion.geometry?.firstMaterial?.lightingModel = .physicallyBased
    
            let comeClose = SCNAction.move(to: SCNVector3(x: 0, y: 0, z: 0), duration: TimeInterval(self.viewModel.shipSpeed))

            avion.runAction(comeClose) { [weak self] in
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Game Over!!!", message: "Un avión ha chocado contigo.", preferredStyle: .alert)
                    let closeAction = UIAlertAction(title: "Close", style: .default) { [weak self] _ in
                        self?.gameOver()
                    }
                    
                    alert.addAction(closeAction)
                    if self?.viewModel.gameIsOver == false {
                        self?.viewModel.setGameIsOver(isOver: true)
                        self?.present(alert, animated: true) { [weak self] in
                            self?.pauseScene()
                            
                        }
                    }
                }
            }
            
            self.sceneView.prepare([avion]) { _ in
                self.sceneView.scene.rootNode.addChildNode(avion)
            }
            
            addNewAmmoBox()
        }
        
    }
    
    private func addNewAmmoBox() {
        if (self.viewModel.ammoBoxes.count < self.viewModel.maxAmmoBoxesCount) {
            let ammoBox = AmmoBox()
            let x = CGFloat.random(in: 0 ... 2)
            let y = CGFloat.random(in: 0 ... 1)
            let z = CGFloat.random(in: -2 ... -1.5)
            
            ammoBox.position = SCNVector3(x, y, z)
            ammoBox.geometry?.firstMaterial?.lightingModel = .physicallyBased
            self.viewModel.addAmmoBox(ammoBox: ammoBox)
            
            self.sceneView.prepare([ammoBox]) { _ in
                self.sceneView.scene.rootNode.addChildNode(ammoBox)
            }
        }
    }
    
    private func gameOver() {
        self.dismiss(animated: true) { [weak self] in
            guard let score = self?.viewModel.score else {
                return
            }
            self?.viewModel.updateHightScore(score: score)
        }

    }
    
    private func removeBullets() {
        let bullets = self.sceneView.scene.rootNode.childNodes.filter {
            return $0 is Bullet
        }
        
        bullets.forEach {
            $0.removeFromParentNode()
        }
        
    }
}

// MARK: - Extension ARSessionDelegate
extension GameViewController: ARSessionDelegate {
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        // Situamos el avión mirando a la camara
        if let cameraOrientation = session.currentFrame?.camera.transform {
            self.viewModel.aviones.forEach {
                $0.face(to: cameraOrientation)
            }
        }
    }
        
}

// MARK: - Extension SCNPhysicsContactDelegate
extension GameViewController: SCNPhysicsContactDelegate {
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        if contact.nodeA.physicsBody?.categoryBitMask == Collision.ammoBox.rawValue || contact.nodeB.physicsBody?.categoryBitMask == Collision.ammoBox.rawValue {
            AmmoBoxCollision(contact: contact)
        }
            
        // Si la colision es entre la bala y el avión, la procesamos
        if contact.nodeA.physicsBody?.categoryBitMask == Collision.avion.rawValue || contact.nodeB.physicsBody?.categoryBitMask == Collision.avion.rawValue {
            ShipCollision(contact: contact)
        }
        
        removeBullets()
    }
    
    private func ShipCollision(contact: SCNPhysicsContact) {
                    
        var node: SCNNode!
        if contact.nodeA is Ship && contact.nodeB is Bullet {
            node = contact.nodeA
            contact.nodeB.removeFromParentNode()
        }
        if contact.nodeB is Ship && contact.nodeA is Bullet {
            node = contact.nodeB
            contact.nodeA.removeFromParentNode()
            
        }
                      
        print("Ship Collision")

        // Calculamos el daño
        let damage = 1 * self.viewModel.damageMultiplier
        
        let ships: [Ship] = self.viewModel.aviones.filter {
            return $0 == node
        }

        guard let ship = ships.first else {
            return
        }
        
        print("lifeBar: \(ship.lifeBar)")

        // Restamos daño a la barra de vida
        ship.remainingLife -= damage

        if (ship.remainingLife <= 0) {
            
            // Añadimos 20 puntos a la puntuación
            self.viewModel.increaseScore(with: 20)
            DispatchQueue.main.async {
                self.scoreLabel.text = "\(self.viewModel.score)"
            }
            
            print("remainingLife: \(ship.remainingLife ?? 0)")
            // Se produce la explosión
            Explossion.show(with: node, in: sceneView.scene)
            
            // El avion desaparece borrando todos el nodo adecuado de la escena
            node.removeFromParentNode()
            self.viewModel.removeAviones(scnNode: node)
            
            // Cargamos un nuevo avión
            addNewShip(count: 1)
        } else {
            let percent: Float = Float(ship.remainingLife) * 100 / Float(ship.lifeBar)
            
            // Escalamos la barra de vida según la vida disponible
            ship.lifeBarShape.scale = SCNVector3Make(Float(percent/100), 1, 1)
            
        }

    }
    
    private func AmmoBoxCollision(contact: SCNPhysicsContact) {
        if contact.nodeA is AmmoBox && contact.nodeB is Bullet {
            contact.nodeA.removeFromParentNode()
            contact.nodeB.removeFromParentNode()
            viewModel.removeBox(scnNode: contact.nodeA)
        }
        
        if contact.nodeB is AmmoBox && contact.nodeA is Bullet {
            contact.nodeB.removeFromParentNode()
            contact.nodeA.removeFromParentNode()
            viewModel.removeBox(scnNode: contact.nodeB)
        }
        
        print("AmmoBox Collision")

        viewModel.addAmmo2()
        DispatchQueue.main.async {
            self.ammo2Label.text = "\(self.viewModel.ammo2)"
            self.ammo2Button.isEnabled = true
        }
    }
}

protocol GameViewControllerProtocol: class {
    func updateHightScore(score: Int)
    func ammo1Tapped()
    func ammo2Tapped()
    func addAmmo2()
    func subAmmo2()
    func removeBox(scnNode: SCNNode)
    func removeAviones(scnNode: SCNNode)
    func increaseScore(with points: Int)
    func addAmmoBox(ammoBox: AmmoBox)
    func addAvion(avion: Ship)
    func setGameIsOver(isOver: Bool)
    func setDamageMultiplier(to: Int)
}
