//
//  GameViewModel.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 24/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import Foundation
import SceneKit

final class GameViewModel {
    // MARK: - Public Properties
    let router: GameRouter
    weak var view: GameViewController?
    var score: Int = 0
    var aviones = [Ship]()
    var ammoBoxes = [AmmoBox]()
    let ammo1: Int = 1
    var ammo2: Int = 4
    var damageMultiplier: Int = 1
    let shipSpeed: Float = 12
    var gameIsOver: Bool = false
    var maxAmmoBoxesCount: Int = 2
    
    // MARK: - Inits
    init(router: GameRouter) {
        self.router = router
        
    }
}

extension GameViewModel: GameViewControllerProtocol {    
    func updateHightScore(score: Int) {
        self.router.updateHightScore(score: score)
    }
    
    func ammo1Tapped() {
        self.damageMultiplier = 1
    }
    
    func ammo2Tapped() {
        self.damageMultiplier = 2
    }
    
    func addAmmo2() {
        self.ammo2 += 2
    }
    
    func subAmmo2() {
        self.ammo2 -= self.damageMultiplier
    }
    
    func removeBox(scnNode: SCNNode) {
        self.ammoBoxes.removeAll {
            return $0 == scnNode
        }
    }
    
    func removeAviones(scnNode: SCNNode) {
        self.aviones.removeAll { ship -> Bool in
            ship as SCNNode == scnNode
        }
    }

    func increaseScore(with points: Int) {
        self.score += points
    }

    func addAmmoBox(ammoBox: AmmoBox) {
        self.ammoBoxes.append(ammoBox)
    }
    
    func addAvion(avion: Ship) {
        self.aviones.append(avion)
    }
    
    func setGameIsOver(isOver: Bool) {
        self.gameIsOver = isOver
    }
    
    func setDamageMultiplier(to value: Int) {
        self.damageMultiplier = value
    }
    
}

protocol GameViewModelProtocol: class {
    func updateHightScore(score: Int)
}
