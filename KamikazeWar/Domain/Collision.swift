//
//  Collision.swift
//  MataAviones
//
//  Created by Santiago Sanchez merino on 13/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import Foundation

struct Collision: OptionSet {
    let rawValue: Int
    
    static let avion = Collision(rawValue: 1 << 0)
    static let bullet = Collision(rawValue: 1 << 1)
    static let gameOver = Collision(rawValue: 1 >> 0)
    static let ammoBox = Collision(rawValue: 1 >> 1)

}
