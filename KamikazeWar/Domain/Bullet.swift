//
//  Bullet.swift
//  MataAviones
//
//  Created by Santiago Sanchez merino on 13/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import ARKit

final class Bullet: SCNNode {

    // MARK: - Private Properties
    private let speed: Float = 9

    // MARK: - Inits
    init(_ camera: ARCamera) {
        super.init()
        
        // Creamos un modelo por código
        let bullet = SCNSphere(radius: 0.02)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.gray
        bullet.materials = [material]
        
        self.geometry = bullet
        
        // Añadimos físicas
        // Creamos la forma del collider
        let shape = SCNPhysicsShape(geometry: bullet)
        // Añadimos el collider al physiscsBody
        self.physicsBody = SCNPhysicsBody(type: .dynamic, shape: shape)
        
        // Creamos el identificador del objeto
        self.physicsBody?.categoryBitMask = Collision.bullet.rawValue
        
        // Definimos el resto de objetos con los que puede colisionar
        self.physicsBody?.contactTestBitMask = Collision.avion.rawValue

        // Aplicar impulso a la bala
        let matrix = SCNMatrix4(camera.transform)
        
        //Trazamos un vector de dirección
        let v = -self.speed
        let dir = SCNVector3(v * matrix.m31, v * matrix.m32, v * matrix.m33)
        
        // Vector de posición que indica el punto de salida de la bala
        let pos = SCNVector3(matrix.m41, matrix.m42, matrix.m43)
        
        self.physicsBody?.applyForce(dir, asImpulse: true)
        self.position = pos
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
