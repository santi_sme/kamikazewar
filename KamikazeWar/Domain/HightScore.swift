//
//  HightScore.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 18/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import Foundation

struct HightScore {
    // MARK: - Private Properties
    static let hightScoreKey = "HightScore"
    
    // MARK: - Static Methods
    static func save(_ value: Int) {
        UserDefaults.standard.set(value, forKey: hightScoreKey)
    }
    
    static func load() -> Int {
        UserDefaults.standard.integer(forKey: hightScoreKey)
    }
}
