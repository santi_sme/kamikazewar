//
//  Explossion.swift
//  MataAviones
//
//  Created by Santiago Sanchez merino on 13/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import ARKit

struct Explossion {
    static func show(with node: SCNNode, in scene: SCNScene) {
        guard let explossion = SCNParticleSystem(named: "Explossion", inDirectory: nil) else {
            return
        }
        
        let position = node.position
        let translationMatrix = SCNMatrix4MakeTranslation(position.x, position.y, position.z)
        let rotation = node.rotation
        let rotationMatrix = SCNMatrix4MakeRotation(rotation.w, rotation.x, rotation.y, rotation.z)
        let transformMatrix = SCNMatrix4Mult(translationMatrix, rotationMatrix)
        
        explossion.emitterShape = node.geometry
        scene.addParticleSystem(explossion, transform: transformMatrix)
        
    }
    
}
