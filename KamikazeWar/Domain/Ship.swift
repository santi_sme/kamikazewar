//
//  Ship.swift
//  MataAviones
//
//  Created by Santiago Sanchez merino on 12/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import ARKit

final class Ship: SCNNode {
    // MARK: - Private Properties
    var lifeBar: Int = Int.random(in: 1 ... 4)
    var remainingLife: Int!
    lazy var lifeBarShape: SCNNode = {
        let shapePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 1, height: 0.1), cornerRadius: 0)
        let node = SCNNode(geometry: SCNShape(path: shapePath, extrusionDepth: 0))
        let material = node.geometry?.firstMaterial
        material?.diffuse.contents = UIColor.green
        material?.isDoubleSided = true
        node.name = "LifeBar"
        
        return node
    }()
    
    // MARK: - Inits
    override init() {
        self.remainingLife = self.lifeBar
        super.init()
        
        let avion = SCNScene(named: "ship.scn")
        guard let node = avion?.rootNode else {
            return
        }
        
        node.name = "Avion"
        node.geometry?.firstMaterial?.lightingModel = .physicallyBased

        self.lifeBarShape.position = SCNVector3Make(-0.5, 0.3, -1)
        
        self.addChildNode(node)
        node.addChildNode(self.lifeBarShape)

        // Añadimos físicas al avión
        let shape = SCNPhysicsShape(node: node, options: nil)
        self.physicsBody = SCNPhysicsBody(type: .static, shape: shape)
        self.physicsBody?.isAffectedByGravity = false
        
        // Identificadores de objetos
        // Es necesario identificar cada uno de los objetos que van a interactuar con otros
        self.physicsBody?.categoryBitMask = Collision.avion.rawValue
        
        // Definimos el resto de objetos con los que puede colisionar
        self.physicsBody?.contactTestBitMask = Collision.bullet.rawValue
        
        // Acciones
        let hoverUp = SCNAction.moveBy(x: 0, y: 0.2, z: 0, duration: 2.5)
        let hoverDown = SCNAction.moveBy(x: 0, y: -0.2, z: 0, duration: 2.5)

        let hoverSequence = SCNAction.sequence([hoverUp, hoverDown])
        let hoverForEver = SCNAction.repeatForever(hoverSequence)
        
        self.runAction(hoverForEver)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    func face(to objectOrientation: simd_float4x4) {
        var transform = objectOrientation
        transform.columns.3.x = self.position.x
        transform.columns.3.y = self.position.y
        transform.columns.3.z = self.position.z

        self.transform = SCNMatrix4(transform)
    }
    
}
