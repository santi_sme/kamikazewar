//
//  AmmoBox.swift
//  KamikazeWar
//
//  Created by Santiago Sanchez merino on 23/11/2019.
//  Copyright © 2019 Santiago Sanchez Merino. All rights reserved.
//

import Foundation
import ARKit

final class AmmoBox: SCNNode {
    // MARK: - Private Properties
    lazy private var ammoBox: SCNNode = {
        
        let node = SCNNode(geometry: SCNBox(width: 0.2, height: 0.2, length: 0.2, chamferRadius: 0.02))
        node.name = "AmmoBox"
        let material = node.geometry?.firstMaterial
        material?.diffuse.contents = UIImage(named: "AmmoBoxTexture")
        material?.lightingModel = .physicallyBased
        
        return node
    }()
    
    // MARK: - Inits
    override init() {
        super.init()
        
        self.addChildNode(self.ammoBox)
        
        // Añadimos físicas
        let shape = SCNPhysicsShape(node: self.ammoBox, options: nil)
        self.physicsBody = SCNPhysicsBody(type: .kinematic, shape: shape)
        self.physicsBody?.isAffectedByGravity = false
        
        self.physicsBody?.categoryBitMask = Collision.ammoBox.rawValue
        
        // Definimos el resto de objetos con los que puede colisionar
        self.physicsBody?.contactTestBitMask = Collision.bullet.rawValue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

